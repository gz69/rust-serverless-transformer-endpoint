# Use the official Rust image as a parent image
FROM rust:1.75 as builder

# Set the working directory
WORKDIR /usr/src/translation-serv

# Copy the current directory contents into the container
COPY . .

# Build the application in release mode
RUN cargo install --path .

# Set the startup command to run the application
CMD ["translation-serv"]

