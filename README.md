
# Rust Serverless Transformer Endpoint

## Project Overview
This project provides a serverless translation service using a Rust implementation on AWS Lambda. It leverages the Hugging Face `rust-bert` library and M2M100 model to translate text inputs via a REST API.

## Table of Contents
- [Project Overview](#project-overview)
- [Requirements](#requirements)
- [Installation and Usage](#installation-and-usage)
- [Development Process](#development-process)
- [Deliverables](#deliverables)
- [Testing and Examples](#testing-and-examples)
- [Documentation](#documentation)

## Requirements
- Docker
- AWS Lambda
- Rust and Cargo

## Installation and Usage
1. **Environment Setup**:
   - Install Rust and Cargo from the official Rust website.
   - Ensure Docker is installed and running on your system.
2. **Repository**:
   - Clone the project: `git clone https://gitlab.com/yourusername/project.git`
3. **Running the Project**:
   - Locally: `cargo run`
   - Using Docker:
     - Build: `docker build -t translation-service .`
     - Run: `docker run -p 3030:3030 translation-service`
   - Via cURL to test the deployed Lambda function: `https://yourlambdaurl.lambda-url.us-east-1.on.aws/`

## Development Process
The development involved setting up a Rust environment, implementing translation functionalities using `rust-bert`, and deploying the application to AWS Lambda. Extensive testing was performed to ensure accuracy and reliability.

## Deliverables
- Dockerfile
- Rust source code (`src/main.rs`)
- Screenshots of the AWS Lambda console and cURL requests

## Testing and Examples
Use the following cURL command to test the endpoint:
```bash
curl -X POST -H "Content-Type: application/json" -d '{"text": "Hello, world!", "lang": "es"}' https://yourlambdaurl.lambda-url.us-east-1.on.aws/
```
